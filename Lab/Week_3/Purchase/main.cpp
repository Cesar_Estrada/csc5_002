/* 
 * File:   main.cpp
 * Author: Cesar
 * ID: 2498637
 * Week Lab 3: Purchasing problem
 * I certify that this is my own work and code
 * 
 * Created on March 10, 2014, 4:33 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Get the amount owed by user
    cout << "Enter the amount owed: " << endl;
    double owe;
    cin >> owe;
    
    // Get the tender
    cout << "Entered the paid amount: " << endl;
    double tender;
    cin >> tender;
    
    // get change from user
    double change = tender - owe;
    
        change *= 100; // Convert to pennies
        // add .05 to change the couble int conversion
        int changeInPennies = static_cast<int>(change + .05);
        cout << "Change in pennies: " << changeInPennies << endl;
        
        // Calculate total amount of dollars
        // Create constants representing currency
        const int DOLLAR = 100;
        const int QUARTER = 25;
        const int DIME = 10;
        const int NICKEL = 5;
        const int PENNY = 1;
        
        // Calculate the number of dollars
        int numDollars = changeInPennies / DOLLAR;
        
        // Get remaining change
        changeInPennies %= DOLLAR;
        
        // Quarters
        int numQuarters = changeInPennies / QUARTER;
        changeInPennies %= QUARTER;
        
        // Dimes
        int numDimes = changeInPennies / DIME;
        changeInPennies %= DIME;
        
        // Nickels
        int numNickels = changeInPennies / NICKEL;
        changeInPennies %= NICKEL;
        
      // Pennies
        int numPennies = changeInPennies;
        
        // Output each amount
        cout << "Number of dollars: " << numDollars << endl;
        cout << "Number of quarters: " << numQuarters << endl;
        cout << "Number of dimes: " << numDimes << endl;
        cout << "Number of nickels: " << numNickels << endl;
        cout << "Number of pennies: " << numPennies << endl;
        
    
    return 0;
}

