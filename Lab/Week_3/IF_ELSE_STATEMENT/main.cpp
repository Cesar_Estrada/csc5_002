/* 
 * File:   main.cpp
 * Author: Cesar
 *
 * Created on March 3, 2014, 2:29 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string name = "Cesar Estrada";
    
   cout << name.substr(2,5) << endl;
   
   // Should Expect  sar E
   // Gets a substring starting at location 2, length
   // 0-based indexing
   cout << name.substr(2,7) << endl;
   
   //Using Find member function
   // Should return location of e at location 2
   cout << name.find ("e") << endl;
   
   // string not located in the name
   // example of what happens when you are trying to find
   // a word that isn't there
   cout << name.find ("asjbfakbf") << endl;
    
   int loc = name.find("asjbfakbf");
   
   //incrememnt largest location
   loc++;
   // Should outpit/reset to 0
   cout << "loc: " << loc << endl;
   
   // Output the length and size of the name
   cout << "Length: " << name.length() << endl;
   //---------------------------------------------------------
   // IF Statements
   
   int x = 10;
   cout << "X Before If: " << x << endl;
   if (x > 5)
   {
       x+= 5; // Add 5 to x only if x is greater than 5
   }
   cout << "X After If: " << x << endl;
   
    
   // If-Else block
   int  y = 5;
   cout << "Y Before If: " << y <<endl;
   if (y < 5)
   {
       y += 5;
       
   }
   else
   { 
       y -= 5; //Subtract 5 from y
   }
   cout << "Y After If: " << y << endl;
    return 0;
}

