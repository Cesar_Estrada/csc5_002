/* 
 * File:   main.cpp
 * Author: Cesar
 *
 * Created on March 12, 2014, 3:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Determine if a number is positive/negative
    // Determine if a number is even/odd
    cout << "Enter a number: " << endl;
    int num;
    cin >> num;
    
    // First way - without nested statements
    // Checking if pos/neg first
    if (num < 0)
    {
        cout << "Negative number " << endl;
        
    }
    
    
    // Check if number is even or odd
    if (num % 2 == 0)
    {
        cout << "Even number " << endl;
        
    }
    else
    {
        cout << "Odd number " << endl;
    }
    
    // Nested if Statement
    if (num < 0)
    {
        cout << "Negative number" << endl;
        
        if (num % 2 == 0)
        {
            cout << "Even number" << endl;
        }
        else
        {
            cout << "Odd number" << endl;
        }
    }
        else
                {
        cout << "Positive number" << endl;
        
        if (num % 2 == 0)
        {
            cout << "Even number" << endl;
        }
        else
        {
            cout << "Odd number" << endl;
        }
        }
        
    // For loop // Iterate/loop 1000 times. Output "i"
        
        for (int i = 0; i < 100; i++)
        {
            cout << "i: " << i << endl;
        }
    
    
    
            
            // user controlled loop
            // prompt the user to enter a number
            // -1 to quit entering
            cout << "Enter a number. Enter -1 to stop" << endl;
        int userNum;
        cin >> userNum;
        
        while (userNum != -1)
        {
            // Out number to user
            cout << "You entered: " << userNum << endl;
            
            // Prompt the user for another number
            cout << "Please enter another number."
                    << " -1 to stop" << endl;
        }
            
    return 0;
}

