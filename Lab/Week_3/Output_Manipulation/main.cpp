/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 3:33 PM
 */

#include <cstdlib>
#include <iostream> // allow i/o
#include <iomanip> // allows for output mainpulation
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // create a constant variable
    const double num = 12.3294;
    
    // Illegal code
    // num = 12;
    // Default is right justified
    cout << setw(10) << num << num << endl;
    
    // One fix
    // cout << setw(10); cout << elft << num << num << endl;
    
    //fix number 2 with << brackets
    cout << setw(10) << left << num << num << endl;
    // 2 Dgits
    cout << setprecision(2);
    cout << setw(10) << left << num <<num << endl;
    
    // 3 Decimal places
    cout << setprecision(3) << fixed;
    cout << setw(10) << left << num << num << endl;
    
    // set fill
    cout << setfill (' ');
    cout << setw(10) << num << num << endl;
    
    // Using two setws
    cout << setw(10) << num << num << setw(10)
            << right << num << endl;
    return 0;
}

