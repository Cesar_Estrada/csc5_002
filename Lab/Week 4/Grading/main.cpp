/* 
 * File:   main.cpp
 * Author: Cesar Estrada
 * ID: 2498637
 * 
 *
 * Created on March 12, 2014, 4:39 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int x;
    cout << "The test has a total of 100 points" << endl;
    cout << "Please enter your score" << endl;
    cin >> x;
    
    if( x > 89)
    {
        cout << "You received an A on your test: Outstanding!!" << endl;       
    }
    else if (x > 79)
    {
        cout << "You received a B on your test. Good Work!" << endl;
        
    }
    else if (x > 69)
    { 
        cout << "You received a C on your test. Nice! Keep up the work!!!"
                << endl;
        
    }
    else if ( x > 59)
    { 
        cout << "You received a D on your test. Not good."
                " Keep on your studies and do better!!" << endl;        
    }
    else if ( x > 0)
    {
        cout << "You received an F on your test."
                " Don't let this happen again" << endl;
    }
    else
    {
        cout << "Why have you not done the test already?!" << endl;
    }
    
    
            
            
    return 0;
}

