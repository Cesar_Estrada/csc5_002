/* 
 * File:   main.cpp
 * Author: Cesar
 *
 * Created on March 10, 2014, 3:03 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

// Prompt the user for an inteher number
    // Always prompt the user
 //   cout << "Enter an integer: " << endl;
    
//    int num;
//    cin >> num;
    
//    // Determine if number is positive or negative
//    if (num > 0)
//    {
//        cout << "Your number is positive" << endl;
//    }
//    else // Ultimatum will be executed if no other
//         // condition is satisfied
//    {
//        cout << "Your number is negative" << endl;
//    }
    
        // If-else block/code
//    if (num >  0)
//    {
//        cout << "Your number is positive" << endl;
//    }
//    else if (num < 0)
//    {
//        cout << "Your number is negative" << endl;
//    }
//    else
//    {
//        cout << "You entered the number 0" << endl;
 //   }
    
    
    // Switch cases
    // used for menus
    
    // User prompt
    cout << "Enter a number between 1-3" << endl;
    
    
    // Assume user is providing an integer
    int switchNum;
    cin >> switchNum;
    
    // Switch on a variable
    switch(switchNum)
    {
        case 1:
            cout << "You entered 1" << endl;
            break;
        case 2:
            cout << "You entered 2" << endl;
            break;
            // Next case is all in one line
        case 3: cout << "You entered 3" << endl; break;
        // Default case (like else in if statement))
        default :
            cout << "An invalid number was entered" << endl;
            break;
    }
            
    // while loop
    int x= 0; //initialization
    
    //while loop condition
    while(x < 10)
    { cout << x << endl; // code which is just output
    x++; //Decrementor (error))
    }
    
    return 0;
}

