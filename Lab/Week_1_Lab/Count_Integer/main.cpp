/* 
 * File:   main.cpp
 * Author: Cesar Estrada
 * ID: 2498637
 * 
 *
 * Created on March 5, 2014, 4:22 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;


int main(int argc, char** argv) {

    double miles, feet, inches, meters;
    cout << "Please input your desired number of meters." << endl;
    cin >> meters;
    cout << endl;
    miles = meters/1609.344;
    feet = meters * 3.298;
    inches = feet * 12;
     cout << "miles: " << fixed << setprecision(2) << miles << endl;
     cout << "feet: " << fixed << setprecision(2) << feet << endl;
     cout << "inches: " << fixed << setprecision(2) << inches << endl;
     
    
    
    
            
    
   
    return 0;
}

