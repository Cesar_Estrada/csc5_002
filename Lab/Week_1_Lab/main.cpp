/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:23 PM
 */

#include <cstdlib> // cstdlib is unneeded
#include <iostream> // iostream is for input and output from console

// std is hot for standard
// all my libraries come from the standard C++ namespace
// also defines context from keywords
using namespace std;

// Disregard this comment, it is just a change it project

// In C++ whitespace is ignored

/*
 * 
 */
// all code is located within main
// there is only one main per program
// code is executed top to bottom, left to right
// Argc and Argv come from outside the program
int main(int argc, char** argv) {

    // Variable definition
    // Date type is a string
    // Variable name is message
    // Assigning "Hello World" to message
    //string message = "Hello World";
    
    // All statements end in a semi-colon
    // Cout means console output
    // << is the stream operator
    // "Hello WOrld" is the a string literal
    // endl makes a new line
    // cout << "Hello World" << endl;
    
    string message; // Variable declaration
    
    // Don't have to initialize message
    // I'm using cin to store a new value
    message = "Hello World"; //Variable Initialization
    
    // Prompt the user
    cout << "Please enter a word" << endl;
            
    //cin gets input from consol and stored into the variable
    // message
    cin >> message;
    
    
    // Output the Variable "message"
    cout << "You entered "  << message << endl;
    
    // If code gets here, then it executed correctly
    // Return flag
    return 0;
}// All code is inside main curly brackets

