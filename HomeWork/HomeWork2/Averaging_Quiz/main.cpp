/* 
 * File:   main.cpp
 * Author: Cesar
 * ID: 2498637
 * Homework 2: Problem 3
 * Created on March 8, 2014, 8:44 PM
 * I certify that this is my own code and work
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char** argv) {

    cout << "Name       Quiz 1  Quiz 2  Quiz 3  Quiz 4" << endl;
    cout << "----       ------  ------  ------  ------" << endl;
    
    string John, Mary, Matthew, Average;
    double a, b, c, d, average1, average2, average3, average4;
        a = 10.0;
        b = 9.0;
        c = 8.0;
        d = 7.0;
        average1 = (a + b + c)/3;
        average2 = (d + c + d)/3;
        average3 = (a + a + b)/3;
        average4 = (b + b + a)/3;  
        
   cout << "John        " << a << "       " << d << "       " << a
           << "       " << b << endl;       
   cout << "Mary        " << b << "        " << c << "       " << a
           << "       " << b << endl;     
   cout << "Matthew     " << c << "        " << d << "       " << b
           << "       " << a << endl;  
   cout << setprecision(3) << "Average     " << average1 << "        "
           << average2 << "    " << average3 << "    " << average4 << endl;
  
    
    return 0;
}

