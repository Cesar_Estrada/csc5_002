/* 
 * File:   main.cpp
 * Author: Cesar Estrada
 * ID: 2498637
 * Homework 2: Problem 4
 * Created on March 8, 2014, 11:08 PM
 * I certify that this is my own code and work
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;





int main(int argc, char** argv) {
    
    cout << "You are now playing Mad Lib. Have fun" << endl;

        string a, b, c, d, e, f, g;
        cout << "Please enter a name: ";
        cin >> a;
        cout << "Please enter another name: ";
        cin >> b;
        cout << "Please enter a food: ";
        cin >> c;
        cout << "Please enter a number between 100 and 120: ";
        cin >> d;
        cout << "Please enter an adjective: ";
        cin >> e;
        cout << "Please enter a color: ";
        cin >> f;
        cout << "Please enter an animal: ";
        cin >> g;
        
        cout << "Dear " << a << "," << endl;
        cout << endl;
        cout << "I am sorry that I am unable to turn in my homework at "
                "this time. First, \n" << "I ate a rotten " << c
                << ", which made me "
                "turn " << f << " and extremely ill.\n";
        cout << "I came down with a fever of " << d << ". Next my " << e
                << " pet " << g << " must have smelled\n"
                << " the remains of the "
                << c << " on my homework because he ate it. ";
        cout << "I am currently rewriting\n"
                << " my homework and hope you will accept"
                " it late." << endl;
        cout << endl;
        cout << "Sincerely," << endl;
        cout << b << endl;
                
                

    return 0;
}

